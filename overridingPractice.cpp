﻿#include <iostream>
#include <string>


class Animal
{
public:
	virtual void Voice() {
		std::cout << "???" << "!\n";
	}
};


class Dog : public Animal
{
public:
	void Voice() override {
		std::cout << "Woof!\n";
	}
};


class Cat : public Animal
{
public:
	void Voice() override {
		std::cout << "Meow!\n";
	}
};


class Duck : public Animal
{
public:
	void Voice() override {
		std::cout << "Quack!\n";
	}
};


int main()
{
	const int size{ 3 };
	// создаем массив указателей
	Animal** array = new Animal* [size];
	array[0] = new Dog();
	array[1] = new Cat();
	array[2] = new Duck();

	for (int i = 0; i < size; ++i)
	{
		array[i]->Voice();
	}
}